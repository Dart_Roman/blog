<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const STATUS_DRAFT      = 1;
    const STATUS_MODERATION = 2;
    const STATUS_PUBLISHED  = 3;
    const STATUS_DECLINED   = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header', 'body', 'status', 'author_id',
    ];

    public function getActionsAttribute () {
        switch ($this->status) {
            case self::STATUS_DRAFT:
                return 'Edit Publish';
            case self::STATUS_MODERATION:
                return '-';
            case self::STATUS_PUBLISHED:
            case self::STATUS_DECLINED:
                return 'Remove';
        }
    }

    public function getStatusNameAttribute () {
        switch ($this->status) {
            case self::STATUS_DRAFT:
                return 'Draft';
            case self::STATUS_MODERATION:
                return 'On moderation';
            case self::STATUS_PUBLISHED:
                return 'Published';
            case self::STATUS_DECLINED:
                return 'Declined';
        }
    }
}
