<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class MeController extends Controller
{
    /** @var int */
    const PAGINATION_COUNT = 5;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'DESC')
            ->paginate(self::PAGINATION_COUNT);

        return view('me.index', compact('posts'));
    }
}
