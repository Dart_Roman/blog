<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('posts.index');
    }

    /**
     * Show the application dashboard.
     * @var PostRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(PostRequest $request)
    {
        Post::create([
            'header'    => $request->header,
            'body'      => $request->body,
            'status'    => Post::STATUS_DRAFT,
            'author_id' => auth()->user()->id,
        ]);

        return redirect()->route('me.index');
    }
}
