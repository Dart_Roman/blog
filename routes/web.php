<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'verified'], function () {
    Route::get('/me', 'MeController@index')->name('me.index');
    Route::get('/posts', 'PostController@index')->name('posts.index');
    Route::post('/posts', 'PostController@create')->name('posts.create');
});
