@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-info rounded shadow-sm">
        <h3 class="mb-0 text-white lh-100">Home page</h3>
    </div>

    <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h4 class="border-bottom border-gray pb-2 mb-2">About me</h4>
        <div class="text-muted">
            <strong>My email: </strong> {{ Auth::user()->email }}
        </div>
        <div class="text-muted">
            <strong>My name: </strong> {{ Auth::user()->name }}
        </div>
        <div class="text-muted">
            <strong>My rating: </strong> 0
        </div>
    </div>

    <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h4 class="border-bottom border-gray pb-2 mb-0">My publications:</h4>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Header</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($posts) === 0)
                        <tr>
                            <td colspan="4">No post has been found</td>
                        </tr>
                    @else
                        @foreach($posts as $post)
                            <tr>
                                <td>{{ $post->created_at }}</td>
                                <td>{{ $post->header }}</td>
                                <td>{{ $post->statusName }}</td>
                                <td>{{ $post->actions }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <div class="float-left">
                {{ $posts->links() }}
            </div>
            <div class="float-right">
                <a href="{{ route('posts.index') }}">
                    <button type="button" class="btn btn-success">New post</button>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
