@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-info rounded shadow-sm">
        <h3 class="mb-0 text-white lh-100">New post</h3>
    </div>

    <div class="my-3 p-3 bg-white rounded shadow-sm">
        <form action="{{ route('posts.create') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="exampleFormControlInput1">Header</label>
                <input name="header" type="text" class="form-control" placeholder="Header text input">
            </div>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Tags</label>
                <select name="tags" class="form-control" multiple>
                    <option value="1">Tag 1</option>
                    <option value="2">Tag 2</option>
                    <option value="3">Tag 3</option>
                    <option value="4">Tag 4</option>
                    <option value="5">Tag 5</option>
                </select>
            </div>

            <div class="form-group">
                <label for="exampleFormControlTextarea1">Message</label>
                <textarea name="body" class="form-control" rows="8" id="summernote"></textarea>
            </div>

            <div class="table-responsive">
                <div class="float-right">
                    <button class="btn btn-success">Create</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
